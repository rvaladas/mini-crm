# Mini-CRM

## How to Install

- Clone the repo : `git clone git@bitbucket.org:rvaladas/mini-crm.git`
- Run `composer install` on your project root
- Copy `cp .env.example .env`
- Create MySQL database and edit your .env file
- Run `$ php artisan migrate --seed`
- Run `$ php artisan serve` to start a local server
- Login:
    - email : `admin@admin.com`
    - password : `password`
