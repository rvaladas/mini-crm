<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'email' => $faker->email,
        'website' => $faker->url,
    ];
});
