<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Employee::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $this->faker->word,
        'last_name' => $this->faker->word,
        'email' => $this->faker->email,
        'phone' => $this->faker->phoneNumber
    ];
});
