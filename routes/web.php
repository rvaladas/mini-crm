<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('admin', function () {
    return view('layouts/admin_template');
});

Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'auth'], function () {
        // Employee Routes
        Route::get('/employee', 'EmployeeController@index')->name('employee');
        Route::get('/employee/create', 'EmployeeController@create')->name('employee.create');
        Route::get('/employee/edit/{employee}', 'EmployeeController@edit')
            ->name('employee.edit');
        Route::get('/employee/{employee}', 'EmployeeController@show')
            ->name('employee.show');
        Route::post('/employee/store', 'EmployeeController@store')->name('employee.store');
        Route::put('/employee/{employee}', 'EmployeeController@update')
            ->name('employee.update');
        Route::get('/employee/delete/{employee}', 'EmployeeController@delete')
            ->name('employee.delete');

        // Company Routes
        Route::get('/company', 'CompanyController@index')->name('company');
        Route::get('/company/create', 'CompanyController@create')->name('company.create');
        Route::post('/company/store', 'CompanyController@store')->name('company.store');
        Route::get('/company/edit/{company}', 'CompanyController@edit')
            ->name('company.edit');
        Route::get('/company/{company}', 'CompanyController@show')
            ->name('company.show');
        Route::post('/company/store', 'CompanyController@store')->name('company.store');
        Route::put('/company/{company}', 'CompanyController@update')
            ->name('company.update');
        Route::get('/company/delete/{company}', 'CompanyController@delete')
            ->name('company.delete');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
