@extends('layouts.admin_template')

@section('htmlheader_title')
    Edit company {{ $company->name }}
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <form class="form-horizontal" method="POST" action="{{ route('company.update', $company->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <div class="form-line">
                                    <input id="name" type="text" class="form-control" name="name"
                                           value="{{ $company['name'] }}" autofocus>
                                </div>
                                @if ($errors->has('name'))
                                    <span class="help-block">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <div class="form-line">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ $company['email'] }}">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Website</label>

                            <div class="col-md-6">
                                <div class="form-line">
                                    <input id="website" type="text" class="form-control" name="website"
                                           value="{{ $company['website'] }}">
                                </div>
                                @if ($errors->has('website'))
                                    <span class="help-block">
										<strong>{{ $errors->first('website') }}</strong>
									</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ route('company') }}" type="submit" class="btn btn-info">
                                    Back
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
