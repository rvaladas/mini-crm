@extends('layouts.admin_template')

@section('htmlheader_title')
    Companies
@endsection

@section('content')
    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table table table-striped table-bordered" id="datatable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Website</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($companies as $company)
                        <tr role="row">
                            <td><a href="{{ route('company.show', $company->id) }}">{{ $company->name }}</a></td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->website }}</td>
                            <td>
                                <a href="{{ route('company.show', $company->id) }}">Details</a> |
                                <a href="{{ route('company.edit', $company->id) }}">Edit</a> |
                                <a href="{{ route('company.delete', $company->id) }}" data-method="delete">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset('/assets/datatables/jquery.dataTables.js') }}"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function () {
            jQuery('#datatable').DataTable({
                "scrollX": true
            });
        });
    </script>
@endsection
