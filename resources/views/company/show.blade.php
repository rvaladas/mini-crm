@extends('layouts.admin_template')

@section('htmlheader_title')
    Details of Company {{ $company->name }}
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <form class="form-horizontal">
                        @isset($company->logo)
                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Logo</label>
                            <div class="col-md-6">
                                <div class="form-line">
                                    <img src="/storage/{{ $company->logo }}" alt="{{ $company->name }}'s Logo" width="100" height="100">
                                </div>
                            </div>
                        </div>
                        @endisset
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <div class="form-line">
                                    <input readonly id="name" type="text" class="form-control" name="name" value="{{ $company['name'] }}" autofocus>
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>
                            <div class="col-md-6">
                                <div class="form-line">
                                    <input  readonly id="email" type="email" class="form-control" name="email" value="{{ $company['email'] }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label">Website</label>
                            <div class="col-md-6">
                                <div class="form-line">
                                    <input readonly id="website" type="text" class="form-control" name="website" value="{{ $company['website'] }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a href="{{ route('company') }}" type="submit" class="btn btn-info">
                                    Back
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
