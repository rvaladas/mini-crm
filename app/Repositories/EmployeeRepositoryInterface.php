<?php

namespace App\Repositories;

interface EmployeeRepositoryInterface
{
    public function create($attributes);

    public function all($columns = array('*'), $orderBy = 'id', $sortBy = 'desc');
}
