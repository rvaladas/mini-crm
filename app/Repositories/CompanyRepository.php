<?php

namespace App\Repositories;

use App\Company;

class CompanyRepository implements CompanyRepositoryInterface
{
    protected $model;

    /**
     * CompanyRepository constructor.
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->model = $company;
    }

    /**
     * @param array $attributes
     * @param string $logo
     * @return mixed
     */
    public function create($attributes, $logo = null)
    {
        $attributes['logo'] = $logo;
        return $this->model->create($attributes);
    }

    /**
     * @param array  $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = array('*'), $orderBy = 'id', $sortBy = 'desc')
    {
        return $this->model->orderBy($orderBy, $sortBy)->paginate(Company::PAGINATION_LIMIT);
    }
}
