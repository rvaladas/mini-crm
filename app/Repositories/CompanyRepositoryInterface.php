<?php

namespace App\Repositories;

interface CompanyRepositoryInterface
{
    public function create($attributes, $logo = null);

    public function all($columns = array('*'), $orderBy = 'id', $sortBy = 'desc');
}
