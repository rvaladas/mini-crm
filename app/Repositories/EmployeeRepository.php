<?php

namespace App\Repositories;

use App\Employee;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    protected $model;

    /**
     * EmployeeRepository constructor.
     * @param Employee $employee
     */
    public function __construct(Employee $employee)
    {
        $this->model = $employee;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create($attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * @param array  $columns
     * @param string $orderBy
     * @param string $sortBy
     * @return mixed
     */
    public function all($columns = array('*'), $orderBy = 'id', $sortBy = 'desc')
    {
        return $this->model->orderBy($orderBy, $sortBy)->paginate(Employee::PAGINATION_LIMIT);
    }
}
