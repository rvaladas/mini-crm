<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    const PAGINATION_LIMIT = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'company_id',
        'email',
        'phone'
    ];

    protected $appends = [
        'full_name',
    ];

    /**
     * Get the employee's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Relationship: An Employee belongs to a Company
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
