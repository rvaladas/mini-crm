<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\StoreCompany;
use App\Http\Requests\UpdateCompany;
use App\Repositories\CompanyRepository;
use Illuminate\Contracts\View\View;

class CompanyController extends Controller
{
    private $companyRepository;

    /**
     * CompanyController constructor.
     *
     * @param CompanyRepository $companyRepository
     */
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->middleware('auth');
        $this->companyRepository = $companyRepository;
    }

    /**
     * @return View
     */
    public function index()
    {
        $companies = $this->companyRepository->all(null, 'created_at');

        return view('company/index', $companies)->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('company/create');
    }

    /**
     * Show specific company
     *
     * @param Company $company
     * @return View
     */
    public function show(Company $company)
    {
        return view('company/show', compact('company'));
    }

    /**
     * Store new company
     *
     * @param StoreCompany $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompany $request)
    {
        try {
            $fileNameToStore = null;
            if ($request->hasFile('logo')) {
                $filenameWithExt = $request->file('logo')->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $request->file('logo')->getClientOriginalExtension();
                $fileNameToStore = $filename . '_' . time() . '.' . $extension;
                $request->file('logo')->storeAs('', $fileNameToStore);
            }

            $company = $this->companyRepository->create($request->all(), $fileNameToStore);

            $response = $company->name . " successfully created!";
        } catch (\Throwable $t) {
            $response = redirect()->back()->withErrors($t->getMessage());
        }

        return redirect()->route('company')->withSuccess($response);
    }

    /**
     * Edit form
     *
     * @param Company $company
     * @return View
     */
    public function edit(Company $company)
    {
        return view('company/edit', compact('company'));
    }

    /**
     * Update company
     *
     * @param UpdateCompany $request
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompany $request, Company $company)
    {
        $company->update($request->all());
        return redirect()->route('company')->withSuccess($company->name . ' has been updated!');
    }

    /**
     * Delete company
     *
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function delete(Company $company)
    {
        $company->delete();
        $message = $company->name . " successfully deleted!";
        return redirect()->route('company')->withSuccess($message);
    }
}
