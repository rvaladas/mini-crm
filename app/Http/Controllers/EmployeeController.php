<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use App\Http\Requests\StoreEmployee;
use App\Http\Requests\UpdateEmployee;
use App\Repositories\EmployeeRepository;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Redirect;

class EmployeeController extends Controller
{
    private $employeeRepository;

    /**
     * EmployeeController constructor.
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->middleware('auth');
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * @return View
     */
    public function index()
    {
        $employees = $this->employeeRepository->all(null, 'created_at');
        return view('employee/index', $employees)->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $companies = Company::all();
        return view('employee/create', compact('companies'));
    }

    /**
     * Store new employee
     *
     * @param StoreEmployee $request
     * @return Redirect
     */
    public function store(StoreEmployee $request)
    {
        try {
            $employee =  $this->employeeRepository->create($request->all());
            $response = redirect()->route('employee')->withSuccess($employee->fullname . ' has been created!');
        } catch (\Throwable $t) {
            $response = redirect()->back()->withErrors($t->getMessage());
        }

        return $response;
    }

    /**
     * Show specific employee
     *
     * @param Employee $employee
     * @return View
     */
    public function show(Employee $employee)
    {
        return view('employee/show', compact('employee'));
    }

    /**
     * Edit form
     *
     * @param Employee $employee
     * @return View
     */
    public function edit(Employee $employee)
    {
        $companies = Company::all();
        return view('employee/edit', compact('employee', 'companies'));
    }

    /**
     * Update employee
     *
     * @param UpdateEmployee $request
     * @param Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployee $request, Employee $employee)
    {
        $employee->update($request->all());
        return redirect()->route('employee')->withSuccess($employee->fullname . ' has been updated!');
    }

    /**
     * Delete employee
     *
     * @param Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function delete(Employee $employee)
    {
        $employee->delete();
        $message = $employee->fullname . " successfully deleted!";
        return redirect()->route('employee')->withSuccess($message);
    }
}
