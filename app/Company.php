<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    const PAGINATION_LIMIT = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'logo',
        'website',
    ];

    /**
     * Relationship: Company hasMany Employee
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
