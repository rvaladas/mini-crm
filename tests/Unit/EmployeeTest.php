<?php

namespace Tests\Unit;

use App\Employee;
use App\Company;
use App\Repositories\EmployeeRepository;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    private $data;

    public function setUp()
    {
        parent::setUp();

        $this->data = [
            'first_name' => $this->faker->word,
            'last_name' => $this->faker->word,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber
        ];
    }

    /** @test */
    public function it_can_create_a_employee()
    {
        // Using the factory
        $company = factory(Company::class)->create();
        $this->data['company_id'] = $company->id;

        $employeeRepository = new EmployeeRepository(new Employee());
        $employee = $employeeRepository->create($this->data);

        $this->assertInstanceOf(Employee::class, $employee);
        $this->assertEquals($this->data['first_name'] . ' ' . $this->data['last_name'], $employee->full_name);
        $this->assertEquals($this->data['email'], $employee->email);
        $this->assertEquals($this->data['phone'], $employee->phone);
        $this->assertEquals($this->data['company_id'], $employee->company_id);
    }
}
