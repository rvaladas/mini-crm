<?php

namespace Tests\Unit;

use App\Company;
use App\Employee;
use App\Repositories\CompanyRepository;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    private $data;

    public function setUp()
    {
        parent::setUp();

        $this->data = [
            'name' => $this->faker->word,
            'email' => $this->faker->email,
            'website' => $this->faker->url
        ];
    }

    /** @test */
    public function it_can_create_a_company()
    {
        $companyRepository = new CompanyRepository(new Company());
        // Using the repository
        $company = $companyRepository->create($this->data);

        $this->assertInstanceOf(Company::class, $company);
        $this->assertEquals($this->data['name'], $company->name);
        $this->assertEquals($this->data['email'], $company->email);
        $this->assertEquals($this->data['website'], $company->website);
    }

    /** @test */
    public function it_can_show_a_company()
    {
        // Using the factory
        $company = factory(Company::class)->create();
        $found = $company->findOrFail($company->id);

        $this->assertInstanceOf(Company::class, $found);
        $this->assertEquals($found->name, $company->name);
        $this->assertEquals($found->email, $company->email);
        $this->assertEquals($found->website, $company->website);
    }

    /** @test */
    public function it_can_delete_a_company_with_employees()
    {
        // Using the factory
        $company = factory(Company::class)->create();
        // Create 2 employees and associate them with the created company
        $employees = new Collection([
            factory(Employee::class)->make(['company_id' => $company->id]),
            factory(Employee::class)->make(['company_id' => $company->id])
        ]);
        $company->employees()->saveMany($employees);

        $delete = $company->delete();
        $this->assertTrue($delete);
    }
}
